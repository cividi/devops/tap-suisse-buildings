"""Custom client handling, including suiss-buildingsStream base class."""

from urllib import request
import io
from zipfile import ZipFile
import csv
from typing import Any, Dict, Optional, Union, List, Iterable

from singer_sdk.streams import Stream

class TapGWRStreamBase(Stream):
    dataset = 'buildings'
    datasets = {
        'buildings': 'gebaeude_batiment_edificio.csv',
        'entrances': 'eingang_entree_entrata.csv',
        'apartments': 'wohnung_logement_abitazione.csv',
        'codes': 'kodes_codes_codici.csv',
    }
    @property
    def full_url(self) -> str:
        return f"{self.config.get('download_base_url')}/{self.config.get('canton').lower()}.zip"
        

    def get_records(self, context: Optional[dict]) -> Iterable[dict]:
        req = request.Request(self.full_url)

        with request.urlopen(req) as resp:
            with ZipFile(io.BytesIO(resp.read())) as zipfile:
                with zipfile.open(self.datasets[self.dataset], "r") as infile:
                    gwr_reader = csv.DictReader(io.TextIOWrapper(infile), delimiter='\t')
                    for row in gwr_reader:
                        yield row
