"""suiss-buildings tap class."""

from typing import List

from singer_sdk import Tap, Stream
from singer_sdk import typing as th  # JSON schema typing helpers
from tap_suisse_buildings.streams import (
    TapSuisseBuildingStream,
    TapSuisseCodesStream,
    TapSuisseEntranceStream,
    TapSuisseApartmentStream
)
STREAM_TYPES = [
    TapSuisseBuildingStream,
    TapSuisseCodesStream,
    TapSuisseEntranceStream,
    TapSuisseApartmentStream
]


class TapSuissebuildings(Tap):
    """suiss-buildings tap class."""
    name = "tap-suisse-buildings"

    config_jsonschema = th.PropertiesList(
        th.Property(
            "canton",
            th.StringType,
            default="ch",
            description="The canton to download (ch for all of Switzerland)",
        ),
        th.Property(
            "start_date",
            th.DateTimeType,
            description="The earliest record date to sync"
        ),
        th.Property(
            "download_base_url",
            th.StringType,
            default="https://public.madd.bfs.admin.ch",
            description="The url for the API service"
        ),
    ).to_dict()

    def discover_streams(self) -> List[Stream]:
        """Return a list of discovered streams."""
        return [stream_class(tap=self) for stream_class in STREAM_TYPES]


if __name__ == "__main__":
    TapSuissebuildings.cli()
